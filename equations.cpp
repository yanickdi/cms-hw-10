#include <cmath>
#include <iostream>
#include <utility>


std::pair<double, double> quadratic(double a, double b, double c) {
	double quadratic, x1, x2;
	quadratic = pow(b, 2.0) - 4 * a * c;
	x1 = (-b + sqrt(quadratic)) / (2 * a);
	x2 = (-b - sqrt(quadratic)) / (2 * a);
	return std::make_pair(x1, x2);
}


int main(int argc, char** argv) {
  if (argc != 4) {
    std::cout << "Usage: " << argv[0] << " a b c\n";
    return 1;
  }
  double a(std::atof(argv[1]));
  double b(std::atof(argv[2]));
  double c(std::atof(argv[3]));

  std::pair<double, double> result(quadratic(a, b, c));
  std::cout << "x1: " << result.first << "; x2: " << result.second << "\n";
  return 0;
}