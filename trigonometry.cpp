#define _USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <iostream>
#include <stdio.h>

int main() {
    std::cout << "radians |  sine | cosine" << std::endl;
    std::cout << "------------------------" << std::endl;
    double rad(.0);
    double sine, cosine;

    while (rad <= 3.14) {
        sine = sin(rad);
        cosine = cos(rad);
        printf("%7.1f |%6.2f | %6.2f\n", rad, sine, cosine);
        rad += .1;
    }
    printf("%7.2f |%6.2f | %6.2f\n", M_PI, sin(M_PI), cos(M_PI));

    return 0;
}
