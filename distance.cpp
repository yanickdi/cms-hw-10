#include <cmath>
#include <iomanip>
#include <string>
#include <iostream>

struct point {
	double x;
	double y;
};

double euclideanDistance(point p1, point p2) {
	return sqrt(pow(p2.x - p1.x, 2.0) + pow(p2.y - p1.y, 2.0));
}

double inputDouble(std::string text) {
	double input;
	std::cout << text;
	std::cin >> input;
	return input;
}

int main() {
	point p1, p2;
	double distance;
	
	p1.x = inputDouble("First point's x-coordinate: ");
	p1.y = inputDouble("First point's y-coordinate: ");
	p2.x = inputDouble("Second point's x-coordinate: ");
	p2.y = inputDouble("Second point's y-coordinate: ");
	distance = euclideanDistance(p1, p2);

	std::cout << std::fixed << std::setprecision(4);
	std::cout << "The euclidean distance between the two points is " << distance << ".\n";
	return 0;
}
