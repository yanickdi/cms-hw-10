﻿#!/usr/bin/env python3
""" This script creates an archiv conaining all relevant homework files as described in the syllabus"""
""" VERSION 0.2 - use at your own risk"""

TEAM = ['Stephan Hermetter', 'Yanick Dickbauer']
ASSIGNMENT = 9
INCLUDE_FILES = ['.*py', '.*txt', '.*csv', '.*json', r'hw(\d)_grader.*pyc']  #regular expressions, full match
EXCLUDE_FILES = ['hw5_grader.cpython-34.py', 'deploy.py']   #also regular expressions full match
ARCHIV_TYPE = 'gztar' #allowed are gztar, bztar, xztar, tar, zip

SEND_MAIL = True
E_MAIL_TO = 'cms@senarclens.eu'
E_MAIL_FROM = 'yanick.dickbauer@edu.uni-graz.at'
E_MAIL_SERVER = 'email.uni-graz.at'
E_MAIL_SERVER_PORT = 587 #using starttls
E_MAIL_USERNAME = 'bzedvz\\10dickba'
E_MAIL_PASSWORD = '' #leave this blank if you want to be asked interactively

import shutil, os, os.path, tempfile, re, getpass, sys
import smtplib
import email.encoders
from email.mime.multipart import MIMEMultipart, MIMEBase

def send_mail(subject, filename, password):
    with smtplib.SMTP(E_MAIL_SERVER, E_MAIL_SERVER_PORT) as smtp:
        ret = smtp.starttls()
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From']  = E_MAIL_FROM
        msg['To'] = E_MAIL_TO
        f = MIMEBase('application', 'octet-stream')
        f.set_payload(open(filename, 'rb').read())
        email.encoders.encode_base64(f)
        f.add_header('Content-Disposition', 'attachment; filename="{}"'.format(os.path.split(filename)[1]))
        msg.attach(f)
        if sys.version_info.major == 3 and sys.version_info.minor >= 5:
            #python v 3.5 and above:
            smtp.login(E_MAIL_USERNAME, password, initial_response_ok=False)
        else:
            #e.g. v3.4:
            smtp.login(E_MAIL_USERNAME, password)
        smtp.sendmail(E_MAIL_FROM, E_MAIL_TO, msg.as_string())


def dirname():
    list = [];
    for str in TEAM:
        list.append('_'.join(str.lower().split()))
    return '{:d}_{:s}'.format(ASSIGNMENT, '_and_'.join(list))

def mail_subject():
    list = [];
    for str in TEAM:
        list.append(' '.join(str.lower().split()))
    return '[ASSIGNMENT {:d}] {:s}'.format(ASSIGNMENT, ', '.join(list))

def file_matches_include_files(filename):
    for regexp in INCLUDE_FILES:
        if re.fullmatch(regexp, filename):
            for regexp_excl in EXCLUDE_FILES:
                if re.fullmatch(regexp_excl, filename):
                    return False
            return True
    return False


if __name__ == '__main__':
    #create temporary folder named dirname()
    tmp_folder = tempfile.mkdtemp()
    hw_folder = os.path.join(tmp_folder, dirname())
    os.mkdir(hw_folder)
    #copy files into hw_folder
    filecount = 0
    for file in os.listdir():
        if file_matches_include_files(file):
            shutil.copy(file, hw_folder)
            filecount += 1
    #create tarfile
    archive_name = shutil.make_archive(dirname(), ARCHIV_TYPE, root_dir=tmp_folder,base_dir=dirname())
    #delete tmp_folder recursively
    shutil.rmtree(tmp_folder)
    print('Done, {:d} files copied to {:s}'.format(filecount, archive_name))
    
    if SEND_MAIL:
        print('I\'ll send the file from {} to {}'.format(E_MAIL_FROM, E_MAIL_TO))
        if not E_MAIL_PASSWORD:
            password = getpass.getpass('Your password for uni account {}: '.format(E_MAIL_USERNAME))
        else:
            password = E_MAIL_PASSWORD
        try:
            send_mail(mail_subject(), archive_name, password)
        except smtplib.SMTPAuthenticationError as e:
            print('wrong username/password')
            sys.exit()
        print('Mail sent, you should get a response mail')
    else:
        print('You have to send your file to {:s}, Subject: "{:s}"'.format(E_MAIL_TO, mail_subject()))