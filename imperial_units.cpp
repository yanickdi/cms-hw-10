#include <iostream>
#include <iomanip>

const double inch_per_meter = 100 / 2.54;

double inch_from_meters(double meters) {
    return (meters * 100) / 2.54;
}

double feet_from_inches(double inches) {
    return inches / 12;
}

double yards_from_feet(double feet) {
    return feet / 3;
}

double miles_from_yards(double yards) {
    return yards / 1760;
}

int main() {
    double meters(0);

    std::cout << "Enter a distance in meters: ";
    std::cin >> meters;

    double inch = inch_from_meters(meters);
    double feet = feet_from_inches(inch);
    double yards = yards_from_feet(feet);
    double miles = miles_from_yards(yards);

    std::cout << std::setprecision(2) << std::fixed;
    std::cout << inch << " inch\n";
    std::cout << feet << " feet\n";
    std::cout << yards << " yards\n";
    std::cout << miles << " miles\n";
    return 0;
}
