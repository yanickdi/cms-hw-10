CFLAGS		:= -g -std=c++11 -Wall -Wextra -Wconversion -O2
LINK_FLAGS	:= 
CC			:= g++

all: simplemath
	
simplemath: simplemath.o
	$(CC) $(CFLAGS) -o simplemath simplemath.o $(LINK_FLAGS)

simplemath.o:
	$(CC) $(CFLAGS) -c simplemath.cpp $(LINK_FLAGS)

circle: circle.o
	$(CC) $(CFLAGS) -o circle circle.o

circle.o:
	$(CC) $(CFLAGS) -c circle.cpp $(LINK_FLAGS)

clean:
	rm *.o
